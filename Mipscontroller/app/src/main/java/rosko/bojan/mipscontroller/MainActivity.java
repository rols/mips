package rosko.bojan.mipscontroller;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.text.DecimalFormat;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import android.os.Handler;

public class MainActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_BLUETOOTH = 555;
    private static final int REQUEST_ENABLE_BT = 123;

    public enum CommandInType {
        POSITION_WRITE(46),
        SCAN_WRITE(47),
        VALUE_WRITE(48),
        READY_WRITE(49);

        CommandInType (int i)
        {
            this.type = i;
        }

        private int type;

        public int getNumericType()
        {
            return type;
        }
    }

    private static final Map<Integer,CommandInType> commandInLookup
            = new HashMap<Integer,CommandInType>();

    static {
        for(CommandInType s : EnumSet.allOf(CommandInType.class))
            commandInLookup.put(s.getNumericType(), s);
    }

    public enum CommandOutType {
        PING_CMD (50),
        SCAN_CMD (51),
        LEFT_CMD(55),
        RIGHT_CMD(56),
        HALT_SCAN_CMD(57),
        POSITION_CMD_START(60);

        CommandOutType (int i)
        {
            this.type = i;
        }

        private int type;

        public int getNumericType()
        {
            return type;
        }
    }

    Filter distanceFilter;
    Filter velocityFilter;

    TextView mDistanceTextView;
    TextView mVelocityTextView;

    Timer mMeasureVelocityTimer;


    static final DecimalFormat df;

    static {
        df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
    }
    // reset each second
    double mTotalDistance = 0.0;
    double mDistance = 0.0;
    double mCurrentSpeed = 0.0;

    private Spinner spinner;
    private Spinner spinnerBlur;
    private Spinner spinnerBlurGaussSize;
    private Spinner spinnerBlurGaussSigma;
    SeekBar slider;
    int currentProgress;

    MyImageView imageView;
    Thread uiThread;

    float dist;
    float velocity;

    private boolean scan_mode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        scan_mode = false;

        distanceFilter = new Filter();
        velocityFilter = new Filter();

        imageView = (MyImageView) findViewById(R.id.imageView);

        slider = (SeekBar) findViewById(R.id.seekBar);
        currentProgress = slider.getProgress();

        ((Button) findViewById(R.id.leftHoldButton)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                onLeftHoldButtonTouch(view, motionEvent);
                return true;
            }
        });
        ((Button) findViewById(R.id.rightHoldButton)).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                onRightHoldButtonTouch(view, motionEvent);
                return true;
            }
        });

        setupSpinners();


        slider.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            long lastSeekBarChange = System.currentTimeMillis();

            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (!b) return;

                long currentTime = System.currentTimeMillis();
                currentProgress = i;

                if (currentTime - lastSeekBarChange > BUTTON_HOLD_UPDATE_TIME) {
                    writePosition(slider.getMax() - currentProgress);
                    lastSeekBarChange = currentTime;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                writePosition(slider.getMax() - currentProgress);
            }
        });

        mDistanceTextView = (TextView) findViewById(R.id.distanceTextView);
        mVelocityTextView = (TextView) findViewById(R.id.velocityTextView);

        setupHandler();

        requestBluetooth();

        mMeasureVelocityTimer = new Timer();

        uiThread = new Thread(new Runnable() {
            @Override
            public void run() {

                while(true) {
                    try {
                        updateView();

                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

            }
        });

        uiThread.start();

    }

    private void setupSpinners() {

        spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.scan_steps_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(0);

        spinnerBlur = (Spinner) findViewById(R.id.spinnerBlur);
        ArrayAdapter<CharSequence> adapterBlur = ArrayAdapter.createFromResource(this,
                R.array.scan_blur_array, android.R.layout.simple_spinner_item);
        adapterBlur.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerBlur.setAdapter(adapterBlur);
        spinnerBlur.setSelection(0);
        spinnerBlur.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int blurSizeInt = Integer.parseInt((String)spinnerBlur.getSelectedItem());
                imageView.blurDots(blurSizeInt);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        AdapterView.OnItemSelectedListener gaussSelectedListener = new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int blurSize = Integer.parseInt((String)spinnerBlurGaussSize.getSelectedItem());
                float blurSigma = Float.parseFloat((String)spinnerBlurGaussSigma.getSelectedItem());
                imageView.gaussianBlurDots(blurSize, blurSigma);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };

        spinnerBlurGaussSize = (Spinner) findViewById(R.id.spinnerBlurGaussSize);
        ArrayAdapter<CharSequence> adapterBlurGaussSize = ArrayAdapter.createFromResource(this,
                R.array.scan_blur_gauss_array, android.R.layout.simple_spinner_item);
        adapterBlur.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerBlurGaussSize.setAdapter(adapterBlurGaussSize);
        spinnerBlurGaussSize.setSelection(0);
        spinnerBlurGaussSize.setOnItemSelectedListener(gaussSelectedListener);

        spinnerBlurGaussSigma = (Spinner) findViewById(R.id.spinnerBlurGaussSigma);
        ArrayAdapter<CharSequence> adapterBlurGaussSigma = ArrayAdapter.createFromResource(this,
                R.array.scan_blur_gauss_sigma_array, android.R.layout.simple_spinner_item);
        adapterBlur.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerBlurGaussSigma.setAdapter(adapterBlurGaussSigma);
        spinnerBlurGaussSigma.setSelection(0);
        spinnerBlurGaussSigma.setOnItemSelectedListener(gaussSelectedListener);
    }


    public void updateView() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mVelocityTextView.setText(df.format(Math.abs(velocity)));
                mDistanceTextView.setText(df.format(dist));

                slider.setProgress(currentProgress);
                imageView.invalidate();
            }
        });
    }

    class PositionUpdateThread extends Thread {

        @Override
        public void run() {
            try {
                while (true) {
                    if ((buttonFlags & BUTTON_HOLD_LEFT) > 0) {
                        //write(CommandOutType.LEFT_CMD);
                        currentProgress--;
                        writePosition(slider.getMax() - currentProgress);
                    } else if ((buttonFlags & BUTTON_HOLD_RIGHT) > 0) {
                        //write(CommandOutType.RIGHT_CMD);
                        currentProgress++;
                        writePosition(slider.getMax() - currentProgress);
                    }

                    Thread.sleep(BUTTON_HOLD_UPDATE_TIME);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    Thread positionUpdateThread;// = new PositionUpdateThread();
    int buttonFlags = 0; // 0x1
    static final int BUTTON_HOLD_LEFT = 0x1;
    static final int BUTTON_HOLD_RIGHT = 0x2;
    static final int BUTTON_HOLD_UPDATE_TIME = 100; // ms

    public void onLeftHoldButtonTouch(View view, MotionEvent motionEvent) {
        onHoldButton(motionEvent.getAction(), BUTTON_HOLD_LEFT);
    }

    public void onRightHoldButtonTouch(View view, MotionEvent motionEvent) {
        onHoldButton(motionEvent.getAction(), BUTTON_HOLD_RIGHT);
    }

    public void onHoldButton(int motionEventAction, int buttonHoldFlag) {
        if (motionEventAction == MotionEvent.ACTION_DOWN || motionEventAction == MotionEvent.ACTION_POINTER_DOWN) {
            buttonFlags |= buttonHoldFlag;

//            Log.e("threading","act down");
//            if (positionUpdateThread != null) {
//                Log.e("threading","interr " + positionUpdateThread.getState() == Thread.State.TERMINATED);
//            }
            if (positionUpdateThread == null){// || positionUpdateThread.isAlive()) {
                positionUpdateThread = new PositionUpdateThread();
                positionUpdateThread.start();
            }
        } else if (motionEventAction == MotionEvent.ACTION_UP || motionEventAction == MotionEvent.ACTION_POINTER_UP) {
            buttonFlags &= ~buttonHoldFlag;

            if (buttonFlags == 0) {
//                Log.e("threading","buttonflags=0");
                positionUpdateThread.interrupt();
                positionUpdateThread = null;
//                Log.e("threading","interr " + positionUpdateThread.isInterrupted());
            }
        }
    }

    public void onLeftClickButton(View v) {
        write(CommandOutType.LEFT_CMD);
    }

    public void onRightClickButton(View v) {
        write(CommandOutType.RIGHT_CMD);
    }

    public void onScanButtonClick(View v) {
        if (!scan_mode) {
            cleanScanDots();
            writeScan();
        }
    }

    public void onCancelScanButtonClick(View v) {
        if (scan_mode) {
            //cleanScanDots();
            imageView.blurDots(Integer.parseInt((String)spinnerBlur.getSelectedItem()));
            write(CommandOutType.HALT_SCAN_CMD);
        }
    }

    public void updatePosition(final String line) {
        float position = Float.parseFloat(line);
        currentProgress = (int) ((1 - position) * slider.getMax());
    }

    public void updateValue(final String line) {
        Log.d("split ", line);
        Log.d("split ", line.split("\\s+")[0] + " " + line.split("\\s+")[1]);
        dist = distanceFilter.filter(Float.parseFloat(line.split("\\s+")[0]));
        int time_ms = Integer.parseInt(line.split("\\s+")[1]);

        long diff = System.currentTimeMillis() - lastTime;
        Log.d("diff ", diff + "");
        lastTime= System.currentTimeMillis();

        velocity = distanceFilter.diff() * 1000f/diff;

        velocity = velocityFilter.filter(velocity);

        if (scan_mode) {
            scan_mode = false;
            imageView.blurDots(Integer.parseInt((String)spinnerBlur.getSelectedItem()));
        }

        imageView.updatePointer((slider.getMax() - currentProgress)/(float)slider.getMax(), dist);

    }


    public void updateScan(final String line) {
        Log.d("split ", line);
        Log.d("split ", line.split("\\s+")[0] + " " + line.split("\\s+")[1]);
        float dist = Float.parseFloat(line.split("\\s+")[0]);
        float position = Float.parseFloat(line.split("\\s+")[1]);
        imageView.addDot(position, dist);
        imageView.updatePointer(position, dist);

        scan_mode = true;

    }

    public void cleanScanDots() {
        imageView.clearDots();
    }

    long lastTime;

    public void processReadLine(String line) {
        Log.d("reader", "read line " + line);

        if (!commandInLookup.containsKey((int)line.charAt(0))) {
            Log.d("reader", "err!");
            return;
        }

        Log.d("reader", (int)line.charAt(0) + "");
        Log.d("reader", commandInLookup.get((int)line.charAt(0)) + "");
        switch(commandInLookup.get((int)line.charAt(0))) {
            case POSITION_WRITE:
                updatePosition(line.substring(1));
                break;
            case VALUE_WRITE:
                updateValue(line.substring(1));
                break;
            case SCAN_WRITE:
                updateScan(line.substring(1));
        }
        double distance = Double.valueOf(line);
        if (mDistance == 0.0) {
            mDistance = distance;
            return;
        }

        synchronized (this) {
            mTotalDistance += Math.abs(distance - mDistance);
        }

        mDistance = distance;
        updateDist(distance);
    }

    public void write(CommandOutType cmd) {
        if (connThread != null) {
            connThread.write(new byte[]{(byte)cmd.getNumericType()});
        }
    }

//    public void write(CommandOutType cmd, float value) {
//        if (connThread != null) {
//            write(cmd);
//            connThread.write(Float.toString(val ue).getBytes());
//        }
//    }

    public void write(CommandOutType cmd, int val) {
        if (connThread != null) {
            write(cmd);
            connThread.write(new byte[]{(byte)val});
        }
    }

    public void writeScan() {
        if (connThread != null) {
            connThread.write(new byte[]{(byte)(CommandOutType.SCAN_CMD.getNumericType() + spinner.getSelectedItemPosition())});
        }
    }

    public void writePosition(int val) {
        if (connThread != null) {
            connThread.write(new byte[]{(byte)(CommandOutType.POSITION_CMD_START.getNumericType() + val)});
        }
    }


    public void updateDist(final double val) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mDistanceTextView.setText(df.format(val));
            }
        });
    }

    @Override
    public void onPause() {
        if (connThread != null)
            connThread.cancel();
        super.onPause();
    }

    @Override
    public void finish() {
        if (connThread != null)
            connThread.cancel();
        super.finish();
    }




















    private void requestBluetooth() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.BLUETOOTH_ADMIN)
                != PackageManager.PERMISSION_GRANTED ||
                ContextCompat.checkSelfPermission(this,
                        Manifest.permission.BLUETOOTH)
                        != PackageManager.PERMISSION_GRANTED ) {
            Toast.makeText(this, "perm not granted!", Toast.LENGTH_SHORT).show();

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH},
                    MY_PERMISSIONS_REQUEST_BLUETOOTH);
        }

        else {
            Toast.makeText(this, "perm granted!", Toast.LENGTH_SHORT).show();
            bluetoothSetup();
        }
    }

    private void setupHandler() {
        mHandler = new Handler();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_BLUETOOTH: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {


                    bluetoothSetup();


                } else {

                    Toast.makeText(this, "You must enable bluetooth!", Toast.LENGTH_SHORT).show();
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private BluetoothAdapter mBluetoothAdapter;
    private ConnectedThread connThread;

    private void bluetoothSetup() {
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Device doesnt support bluetooth!", Toast.LENGTH_SHORT).show();
        }

        if (!mBluetoothAdapter.isEnabled()) {
            Toast.makeText(this, "bt disabled!", Toast.LENGTH_SHORT).show();
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }
        else {
            Toast.makeText(this, "bt enabled!", Toast.LENGTH_SHORT).show();
            bluetoothConnect();
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "blutooth enabled!", Toast.LENGTH_SHORT).show();
                bluetoothConnect();
            } else {
                Toast.makeText(this, "blutooth not enabled!", Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void bluetoothConnect() {
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

        Log.d("btpaired", "pairednum " + pairedDevices.size());
        if (pairedDevices.size() > 0) {
            // There are paired devices. Get the name and address of each paired device.
            for (BluetoothDevice device : pairedDevices) {
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address

                Log.d("btpaired", deviceName + "|" + deviceHardwareAddress);

                if (deviceHardwareAddress.equals("98:D3:32:10:7B:F7")) {
                    connectDevice(device);
                }

            }
        }
    }

    private void connectDevice(BluetoothDevice device) {
        Log.d("btpaired", "yay! connectDevice " + device.getName());

        ConnectThread connectThread = new ConnectThread(device);
        connectThread.start();
    }


    private void manageMyConnectedSocket(BluetoothSocket socket) {

        Log.d("btpaired", "yay! manage ");
        connThread = new ConnectedThread(socket);
        connThread.start();
        mMeasureVelocityTimer.schedule(new TimerTask() {
            @Override
            public void run() {

                synchronized (this) {
                    if (mCurrentSpeed == 0.0) {
                        mCurrentSpeed = mTotalDistance;
                    } else {
                        mCurrentSpeed = (mTotalDistance + mCurrentSpeed) / 2;
                    }

                    mTotalDistance = 0.0;
                }

//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        mVelocityTextView.setText(df.format(mCurrentSpeed));
//                    }
//                });
            }
        }, 1000, 1000);
    }


    private static final UUID MY_UUID = new UUID(123,345);

    private class ConnectThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final BluetoothDevice mmDevice;

        public ConnectThread(BluetoothDevice device) {
            // Use a temporary object that is later assigned to mmSocket
            // because mmSocket is final.
            BluetoothSocket tmp = null;
            mmDevice = device;

            try {
                // Get a BluetoothSocket to connect with the given BluetoothDevice.
                // MY_UUID is the app's UUID string, also used in the server code.

                Log.d(TAG, "trying to connect");
                tmp = device.createInsecureRfcommSocketToServiceRecord(device.getUuids()[0].getUuid());
            } catch (IOException e) {
                Log.e(TAG, "Socket's create() method failed", e);
            }
            Log.d(TAG, "created socket");
            mmSocket = tmp;
        }

        public void run() {
            // Cancel discovery because it otherwise slows down the connection.
            mBluetoothAdapter.cancelDiscovery();

            try {
                // Connect to the remote device through the socket. This call blocks
                // until it succeeds or throws an exception..

                Log.d(TAG, "connect socket");
                mmSocket.connect();

                Log.d(TAG, "connected socket");
            } catch (IOException connectException) {
                // Unable to connect; close the socket and return.
                try {
                    mmSocket.close();
                } catch (IOException closeException) {
                    Log.e(TAG, "Could not close the client socket", closeException);
                }
                return;
            }

            // The connection attempt succeeded. Perform work associated with
            // the connection in a separate thread.
            manageMyConnectedSocket(mmSocket);
        }

        // Closes the client socket and causes the thread to finish.
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close the client socket", e);
            }
        }
    }

    private static final String TAG = "MY_APP_DEBUG_TAG";
    private Handler mHandler; // handler that gets info from Bluetooth service

    // Defines several constants used when transmitting messages between the
    // service and the UI.
    private interface MessageConstants {
        public static final int MESSAGE_READ = 0;
        public static final int MESSAGE_WRITE = 1;
        public static final int MESSAGE_TOAST = 2;

        // ... (Add other message types here as needed.)
    }

    private class ConnectedThread extends Thread {
        private final BluetoothSocket mmSocket;
        private final InputStream mmInStream;
        private final OutputStream mmOutStream;
        private byte[] mmBuffer; // mmBuffer store for the stream

        public ConnectedThread(BluetoothSocket socket) {
            mmSocket = socket;
            InputStream tmpIn = null;
            OutputStream tmpOut = null;

            // Get the input and output streams; using temp objects because
            // member streams are final.
            try {
                tmpIn = socket.getInputStream();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when creating input stream", e);
            }
            try {
                tmpOut = socket.getOutputStream();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when creating output stream", e);
            }

            mmInStream = tmpIn;
            mmOutStream = tmpOut;
        }

        public void run() {
            mmBuffer = new byte[1024];

            BufferedReader streamReader = new BufferedReader(new InputStreamReader(mmInStream));

            // Keep listening to the InputStream until an exception occurs.
            while (true) {
                try {
                    // Read from the InputStream.
                    if (streamReader.ready()) {
                        // In case of "ready" message this will simply skip iteration
                        // thanks to number format exception
                        processReadLine(streamReader.readLine());

                    }

                } catch (NumberFormatException exc) {
                    // nothing
                } catch (IOException e) {
                    Log.d(TAG, "Input stream was disconnected", e);
                    break;
                }
            }
        }

        // Call this from the main activity to send data to the remote device.
        public void write(byte[] bytes) {
            try {
                mmOutStream.write(bytes);

                // Share the sent message with the UI activity.
                Message writtenMsg = mHandler.obtainMessage(
                        MessageConstants.MESSAGE_WRITE, -1, -1, mmBuffer);
                writtenMsg.sendToTarget();
            } catch (IOException e) {
                Log.e(TAG, "Error occurred when sending data", e);

                // Send a failure message back to the activity.
                Message writeErrorMsg =
                        mHandler.obtainMessage(MessageConstants.MESSAGE_TOAST);
                Bundle bundle = new Bundle();
                bundle.putString("toast",
                        "Couldn't send data to the other device");
                writeErrorMsg.setData(bundle);
                mHandler.sendMessage(writeErrorMsg);
            }
        }

        // Call this method from the main activity to shut down the connection.
        public void cancel() {
            try {
                mmSocket.close();
            } catch (IOException e) {
                Log.e(TAG, "Could not close the connect socket", e);
            }
        }
    }
}
