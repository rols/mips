package rosko.bojan.mipscontroller;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

import java.util.ArrayList;

/**
 * Created by rols on 2/8/17.
 */

public class MyImageView extends ImageView {

    PointF sensor;
    float sensorRadius = 100f;
    Paint sensorPaint;

    float maxImageViewDist;
    float minImageViewDist;
    float currentMaxDist;
    float currentMinDist;

    ArrayList<Float> procAngles;
    ArrayList<Float> distances;

    ArrayList<PointF> dots;
    Paint dotPaint;

    PointF pointer;
    float pointerRadius = 10f;
    Paint pointerPaint;

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);

        sensor.y = this.getHeight();
        sensor.x = this.getWidth()/2;
        Log.d("imaz", this.getHeight() + "" );

        minImageViewDist = sensorRadius;
        maxImageViewDist = (this.getWidth()) / 2;
    }

    public void MyImageViewConstructor() {

        pointer = new PointF(0,0);
        pointerPaint = new Paint();
        pointerPaint.setColor(Color.BLUE);

        currentMaxDist = 1;
        currentMinDist = 1;

        procAngles = new ArrayList<>();
        distances = new ArrayList<>();

        dots = new ArrayList<>();


        sensor = new PointF();

        sensorPaint = new Paint();
        sensorPaint.setColor(Color.GRAY);

        dotPaint = new Paint();
        dotPaint.setColor(Color.RED);

    }

    public MyImageView(Context context) {
        super(context);
        MyImageViewConstructor();
    }

    public MyImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        MyImageViewConstructor();
    }

    public MyImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        MyImageViewConstructor();
    }

    public MyImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        MyImageViewConstructor();
    }

    public void updatePointer(float procAngle, float distance) {
        if (distance > currentMaxDist) {
            currentMaxDist = distance;
            updateDots();
        }
        else if (distance < currentMinDist) {
            currentMinDist = distance;
            updateDots();
        }

        pointer = getPointFromPolar(procAngle, distance);
    }

    private void updateDots() {
        dots = new ArrayList<>();

        for (int i = 0 ; i < distances.size(); i ++ ){
            PointF dot = new PointF();

            float angle = (float)(procAngles.get(i) * Math.PI);

            float normalizedDist = (distances.get(i) - currentMinDist) / (currentMaxDist - currentMinDist);
            float realDist = minImageViewDist + (maxImageViewDist - minImageViewDist) * normalizedDist;

            dot.x = sensor.x + (float)(realDist * Math.cos(angle));
            dot.y = sensor.y - (float)(realDist * Math.sin(angle));

            dots.add(dot);
        }
    }

    private PointF getPointFromPolar(float procAngle, float distance) {
        PointF dot = new PointF();

        float angle = (float)(procAngle * Math.PI);

        float normalizedDist = (distance - currentMinDist) / (currentMaxDist - currentMinDist);
        float realDist = minImageViewDist + (maxImageViewDist - minImageViewDist) * normalizedDist;

        dot.x = sensor.x + (float)(realDist * Math.cos(angle));
        dot.y = sensor.y - (float)(realDist * Math.sin(angle));
        return dot;
    }

    public void addDot(float procAngle, float distance) {

        procAngles.add(procAngle);
        distances.add(distance);

        if (distance > currentMaxDist) {
            currentMaxDist = distance;
            updateDots();
        }
        else if (distance < currentMinDist) {
            currentMinDist = distance;
            updateDots();
        }
        else {
            dots.add(getPointFromPolar(procAngle, distance));
        }
    }

    public void gaussianBlurDots(int blurSize, float sigma) {
        ArrayList<PointF> newDots = new ArrayList<>();

        for (int i = 0; i < dots.size(); i ++) {
            float distance = distances.get(i);
            float blurCnt = 1f;
            for (int j = 1; j <= blurSize; j ++) {
                int leftDotIndex = i - j;
                if (leftDotIndex >= 0) {
                    blurCnt += Math.exp(-j*j/sigma/sigma);
                    distance += distances.get(leftDotIndex) * Math.exp(-j*j/sigma/sigma);
                }
                int rightDotIndex = i + j;
                if (rightDotIndex < dots.size()) {
                    blurCnt += Math.exp(-j*j/sigma/sigma);
                    distance += distances.get(rightDotIndex) * Math.exp(-j*j/sigma/sigma);
                }
            }
            distance /= blurCnt;

            newDots.add(getPointFromPolar(procAngles.get(i), distance));
        }

        dots = newDots;
    }

    public void blurDots(int blurSize) {

        ArrayList<PointF> newDots = new ArrayList<>();

        for (int i = 0; i < dots.size(); i ++) {
            float distance = distances.get(i);
            int blurCnt = 1;
            for (int j = 1; j <= blurSize; j ++) {
                int leftDotIndex = i - j;
                if (leftDotIndex >= 0) {
                    blurCnt ++;
                   distance += distances.get(leftDotIndex);
                }
                int rightDotIndex = i + j;
                if (rightDotIndex < dots.size()) {
                    blurCnt ++;
                    distance += distances.get(rightDotIndex);
                }
            }
            distance /= blurCnt;

            newDots.add(getPointFromPolar(procAngles.get(i), distance));
        }

        dots = newDots;
    }

    public void clearDots() {
        dots.clear();
        distances.clear();
        procAngles.clear();
        currentMaxDist = 1;
        currentMinDist = 1;
    }

    @Override
    public void onDraw(Canvas canvas) {

        canvas.drawCircle(sensor.x ,sensor.y, sensorRadius, sensorPaint);

        for (int i = 1; i < dots.size(); i ++) {
            canvas.drawLine(dots.get(i-1).x, dots.get(i-1).y, dots.get(i).x, dots.get(i).y, dotPaint );
        }

        canvas.drawCircle(pointer.x, pointer.y, pointerRadius, pointerPaint);

    }
}
