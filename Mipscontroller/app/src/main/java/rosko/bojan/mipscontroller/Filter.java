package rosko.bojan.mipscontroller;

/**
 * Created by rols on 2/8/17.
 */

public class Filter {

    private static final int NUMLAST = 30;

    float lastVal;
    float lastLastVal;
    float values[] = new float[NUMLAST];
    int currVal = 0;

    float sum = 0;

    private static final float alfa = 0.05f;

    public Filter(){
        lastVal = 0;
        lastLastVal = 0;

        for (int i = 0 ; i  < NUMLAST ; i ++) values[i] = 0;
    }

    public float alfafilter(float val) {
        lastLastVal = lastVal;
        lastVal = lastVal * (1 - alfa) + alfa * val;
        return lastVal;
    }

    public float filter(float val) {
        if (currVal == 0) {
            sum = 0;
            for (int i = 0 ;i < NUMLAST; i ++) {
                sum += values[i];
            }
        }
        sum -= values[currVal];
        sum += val;
        values[currVal] = val;
        currVal++;
        if (currVal == NUMLAST) currVal = 0;

        lastLastVal = lastVal;
        lastVal = sum/NUMLAST;

        return lastVal;
    }

    public float diff() {
        return lastLastVal - lastVal;
    }
}
