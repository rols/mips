// ************************************************** CONSTANTS SECTION

#define PWM_TIME_MIN_RATIO 17.0
#define PWM_TIME_MAX_RATIO 76.0
#define PWM_STEPS 20
#define PWM_POSITION_STEPS_F 100.0f
#define PWM_POSITION_STEPS 100
//#define PWM_SCAN_STEPS 200

#define uS_TO_CM 0.0171821
#define MEASURE_FIX 1.175

#define POSITION_WRITE 46
#define SCAN_WRITE 47
#define VALUE_WRITE 48
#define READY_WRITE 49

#define PING_CMD 50
#define SCAN_CMD_START 51
#define SCAN_CMD_END 54
#define LEFT_CMD 55
#define RIGHT_CMD 56
#define SCAN_HALT_CMD 57

#define POSITION_CMD_START 60
#define POSITION_CMD_END POSITION_CMD_START + PWM_POSITION_STEPS

//#define DEBUG

#define BUTTON_LEFT_DOWN      0x00000001
#define BUTTON_RIGHT_DOWN     0x00000002

#define TIMER_INTERRUPT_MODE 2
#define TIMER_DEBOUNCE_MODE  1

#define PSC_50 36
#define ARR_50 64864

unsigned scan_steps[] = {50, 200, 500, 1000};

//this is config for different times for timer interrupt
//                             5ms       20ms           50ms      100ms    200ms        500ms
unsigned timer_psc[] = {       9,        39,            95,       191,     374,         959   };
unsigned timer_arr[] = {       59999,    59999,         62499,    62499,   63999,       62499   };
int timer_times[]={            5,        20,            50,       100,     200,         500     };

// ************************************************** VARIABLES SECTION

// pin definitions
sbit LD1 at ODR12_GPIOE_ODR_bit;
sbit LD2 at ODR15_GPIOE_ODR_bit;
sbit TRIG at ODR5_GPIOD_ODR_bit;
sbit ECHO at IDR6_GPIOD_IDR_bit;

char out[256];
int time;
unsigned pwm_time;
unsigned pwm_time_init;
unsigned pwm_step;
unsigned pwm_scan_step;
unsigned pwm_max;
unsigned pwm_min;

unsigned timer3_val;
unsigned last_timer3_val;
unsigned button_state; // bit 0 -> button 1, bit 1 -> button 1

unsigned scan_mode;
unsigned position_mode;
unsigned scan_position;

unsigned int bt_input, connected;

// ************************************************** HELPERS SECTION

unsigned init_pwm_timer(){
  // INIT pin
  GPIO_Alternate_Function_Enable(&_GPIO_MODULE_TIM4_CH1_PD12);
  Delay_ms(10);

  // Enable Timer4
  RCC_APB1ENR.TIM4EN = 1;
  // Disable counter
  TIM4_CR1bits.CEN = 0;

  // Set 20ms (50hz) period
  TIM4_PSC = PSC_50;
  TIM4_ARR = ARR_50;

  // division 1
  TIM4_CR1bits.CKD = 0;
  // counting direction
  TIM4_CR1bits.DIR_ = 0;   // UP

  // 0x6 is EDGE pwm / compare mode
  //TIM4_CCMR1_Output &= (~(0x7 << 4));
  OC1CE_TIM4_CCMR1_Output_bit = 1;
  TIM4_CCMR1_Outputbits.OC1M = 6;

  // enable pwm
  TIM4_CCERbits.CC1E = 1;

  // high polarity
  TIM4_CCERbits.CC1P = 0;

  // PRELOAD
  OC1PE_TIM4_CCMR1_Output_bit = 1;

  return TIM4_ARR;
}

void pwm_start() {
  // Starts counter / pwm
  TIM4_CR1bits.CEN = 1;
}

void pwm_set_duty(unsigned pwm_time) {
  TIM4_CCR1 = pwm_time;
}

void pwm_inc(unsigned step) {
     if (pwm_time == pwm_max) {
        return;
     }

     pwm_time += step;
     if (pwm_time > pwm_max) {
        pwm_time = pwm_max;
     }
#ifdef DEBUG
     IntToStr((int)pwm_time, out);
     UART3_Write_Text(out);
     UART3_Write_Text("\r\n");
#endif
     pwm_set_duty(pwm_time);
}
void pwm_dec(unsigned step) {
     if (pwm_time == pwm_min) {
        return;
     }

     pwm_time -= step;
     if (pwm_time < pwm_min) {
        pwm_time = pwm_min;
     }
#ifdef DEBUG
     IntToStr((int)pwm_time, out);
     UART3_Write_Text(out);
     UART3_Write_Text("\r\n");
#endif
     pwm_set_duty(pwm_time);
}

void pwm_position(unsigned int pos) {
     if (pos > PWM_POSITION_STEPS) pos = PWM_POSITION_STEPS;
     pwm_time = pwm_min + (int)((float)(pwm_max - pwm_min) * pos / PWM_POSITION_STEPS_F);
#ifdef DEBUG
     IntToStr((int)pos, out);
     UART3_Write_Text(out);
     UART3_Write_Text(" ");
     IntToStr((int)pwm_time, out);
     UART3_Write_Text(out);
     UART3_Write_Text("\r\n");
#endif
     pwm_set_duty(pwm_time);
}

float readDistance() {
  //Send trigger signal to dist sensor
        TRIG = 1;
        Delay_us(10);                 //10uS Delay
        TRIG = 0;

        while(!ECHO);                 //Waiting for Echo

        time = 0;                       //Timer Starts
        while(ECHO) {
            time++;
            Delay_us(1);
        }
       return (float)(time) * uS_TO_CM * MEASURE_FIX;
}

void update_button_state(unsigned state) {
     if (!scan_mode) {
        last_timer3_val = timer3_val;
        button_state |= state;
     }
}

// ************************************************** INTERUPTS SECTION

float current_position;
char in[32];
int write_position_flag = 0;
int scan_steps_input;

void write_position() {
     current_position = (pwm_time - pwm_min)/(float)(pwm_max-pwm_min);
     FloatToStr(current_position, out);
     UART3_Write(POSITION_WRITE);
     UART3_Write_Text(out);
     UART3_Write_Text("\r\n");
}

void halt_scan() {
     scan_mode = 0;
     pwm_time = (pwm_min + pwm_max)/2;
     pwm_set_duty(pwm_time);

     write_position();
}

void interrupt() iv IVT_INT_USART3 ics ICS_AUTO {
     if (UART3_Data_Ready() == 1) {
         bt_input = UART3_Read();
         if (scan_mode) {
             if (bt_input == SCAN_HALT_CMD) {
                 halt_scan();
             }
          } else {
             switch(bt_input) {
                  case PING_CMD:
                       UART3_Write_text("Ping!\r\n");
                       break;
                  //case SCAN_CMD:
                  //     scan_mode = 1;
                  //     pwm_dec(pwm_time);//minimum
                  //     scan_position = 0;
                       break;
                  case LEFT_CMD:
                       pwm_inc(pwm_step);
                       break;
                  case RIGHT_CMD:
                       pwm_dec(pwm_step);
                       break;
                  default:
                       if (bt_input >= SCAN_CMD_START && bt_input <= SCAN_CMD_END) {
                          scan_mode = 1;
                          pwm_dec(pwm_time);
                          scan_position = 0;
                          scan_steps_input = scan_steps[bt_input - SCAN_CMD_START];
                          pwm_scan_step = (pwm_max - pwm_min + scan_steps_input - 1) / scan_steps_input;
                       }
                       if (bt_input >= POSITION_CMD_START && bt_input <= POSITION_CMD_END) {
                          pwm_position(bt_input - POSITION_CMD_START);
                       }
                       break;
                }


              if (bt_input == LEFT_CMD || bt_input == RIGHT_CMD) {
                 write_position_flag = 1;
                 USART3_CR1bits.RXNEIE = 0;
                 USART3_CR1bits.TXEIE = 1;
              }
          }
     } else if (write_position_flag == 1) {      // TX !!!!! TXE_USART2_SR_bit
        write_position();
        write_position_flag = 0;
        
        USART3_CR1bits.RXNEIE = 1;
        USART3_CR1bits.TXEIE = 0;
     }
}

int deltaTime;
float distance;

void Timer2_interrupt() iv IVT_INT_TIM2 {
  TIM2_SR.UIF = 0;
  //Enter your code here
  //UART3_Write_Text("Timer!\r\n");
  if (scan_mode) {
      distance = readDistance();
      if (distance >= 0) {

        UART3_Write(SCAN_WRITE);
        FloatToStr(distance, out);
        UART3_Write_Text(out);
        UART3_Write_Text(" ");
        FloatToStr(scan_position/(float)scan_steps_input, out);
        UART3_Write_Text(out);
        UART3_Write_Text("\r\n");

        pwm_inc(pwm_scan_step);
        scan_position ++;
        if (scan_position == scan_steps_input) {
           halt_scan();
        }
      }

   } else {
      distance = readDistance();

      deltaTime = timer3_val - last_timer3_val;
      last_timer3_val = timer3_val;

      //todo optimize
      UART3_Write(VALUE_WRITE);
      FloatToStr(distance, out);
      UART3_Write_Text(out);
      UART3_Write_Text(" ");
      /*IntToStr(timer_times[TIMER_INTERRUPT_MODE], out);
      UART3_Write_Text(out);
      UART3_Write_Text(" ");*/
      IntToStr(deltaTime, out);
      UART3_Write_Text(out);
      UART3_Write_Text("\r\n");

      time = 0;
  }
}

void button_left_int() iv IVT_INT_EXTI0 ics ICS_AUTO {
     EXTI_PRbits.PR0 = 1;

     update_button_state(BUTTON_LEFT_DOWN);
}

void button_right_int() iv IVT_INT_EXTI15_10 ics ICS_AUTO {
     EXTI_PRbits.PR10 = 1;

     update_button_state(BUTTON_RIGHT_DOWN);
}

void Timer3_interrupt() iv IVT_INT_TIM3 {
  TIM3_SR.UIF = 0;

  timer3_val++;
  if (last_timer3_val > 0 && timer3_val - last_timer3_val != 0) {
     if (button_state & BUTTON_LEFT_DOWN) {
        pwm_inc(pwm_step);
     }

     if (button_state & BUTTON_RIGHT_DOWN) {
        pwm_dec(pwm_step);
     }

     button_state = 0;
     last_timer3_val = 0;
  }
}

// ************************************************** INITIALIZERS SECTION

void init_timer2(){
  RCC_APB1ENR.TIM2EN = 1;
  TIM2_CR1.CEN = 0;
  TIM2_PSC = timer_psc[TIMER_INTERRUPT_MODE];
  TIM2_ARR = timer_arr[TIMER_INTERRUPT_MODE];
  NVIC_IntEnable(IVT_INT_TIM2);
  TIM2_DIER.UIE = 1;
  TIM2_CR1.CEN = 1;
}
     
void init_btn() {
     /* Init buttons */
     // PORT E, pin 0 -> Button 1 | PORT A, pin 10 -> Button 2
     GPIO_Digital_Input(&GPIOE_BASE, _GPIO_PINMASK_0);
     GPIO_Digital_Input(&GPIOA_BASE, _GPIO_PINMASK_10);

     SYSCFGEN_bit = 1;
     SYSCFG_EXTICR1 = 0x00000004;         // Set PE(EXTI0)
     SYSCFG_EXTICR3 = 0x00000000;         // Set PA(EXTI10)

     EXTI_RTSR = 0x00000000;              // Set interrupt on Rising edge (none)
     EXTI_FTSR = 0x00000401;              // Set Interrupt on Falling edge (PD10)
     EXTI_IMR |= 0x00000401;

     NVIC_IntEnable(IVT_INT_EXTI0);
     NVIC_IntEnable(IVT_INT_EXTI15_10);

     button_state = 0;
}

void init_pins () {
     /* InitGeneral */
     // LED Pins 12(1) and 15(2)
     GPIO_Digital_Output(&GPIOE_BASE, _GPIO_PINMASK_12 | _GPIO_PINMASK_15);

     /* Init distance sensor */
     // pin D6 -> ECHO, pin D5 -> TRIG
     GPIO_Digital_Input(&GPIOD_BASE, _GPIO_PINMASK_6);
     GPIO_Digital_Output(&GPIOD_BASE, _GPIO_PINMASK_5);
     TRIG = 0;

     /* InitBluetoothUART3 */
     UART3_Init_Advanced(9600, _UART_8_BIT_DATA, _UART_NOPARITY, _UART_ONE_STOPBIT, &_GPIO_MODULE_USART3_PD89);
     Delay_ms(100);

     UART3_Enable();

     USART3_CR1bits.RXNEIE = 1;       // enable uart rx interrupt
     USART3_CR1bits.TXEIE = 0;        // enable uart tx interrupts

     NVIC_IntEnable(IVT_INT_USART3);  // interrupt routine
     scan_mode = 0;
     position_mode = 0;
}

void init_PWM() {
     pwm_time_init = init_pwm_timer();

     pwm_min = (pwm_time_init + PWM_TIME_MAX_RATIO - 1) / PWM_TIME_MAX_RATIO;
     pwm_max = pwm_time_init / PWM_TIME_MIN_RATIO;
     pwm_step = (pwm_max - pwm_min + PWM_STEPS - 1) / PWM_STEPS;
     //pwm_scan_step = (pwm_max - pwm_min + PWM_SCAN_STEPS - 1) / PWM_SCAN_STEPS;

     pwm_time = (pwm_min + pwm_max)/2;

     pwm_set_duty(pwm_time);
     pwm_start();
}

//Timer3 Prescaler :0; Preload = 119; Actual Interrupt Time = 1 us
//Place/Copy this part in declaration section
void init_timer3(){
  RCC_APB1ENR.TIM3EN = 1;
  TIM3_CR1.CEN = 0;
  TIM3_PSC = timer_psc[TIMER_DEBOUNCE_MODE];
  TIM3_ARR = timer_arr[TIMER_DEBOUNCE_MODE];
  NVIC_IntEnable(IVT_INT_TIM3);
  TIM3_DIER.UIE = 1;
  TIM3_CR1.CEN = 1;

  timer3_val = 0;
  last_timer3_val = 0;
}

// ************************************************** MAIN SECTION
void main() {

     init_timer3();
     init_pins();
     init_PWM();
     init_timer2();
     init_btn();

     // Light led's
     // Signalize that all is initiated and radar can be used
     LD1 = LD2 = 1;
     Delay_ms(500);
     LD1 = LD2 = 0;
     
#ifdef DEBUG
     UART3_Write_Text("All interrupts initialized, entering WFI mode.\r\n");
#endif
     UART3_Write(READY_WRITE);
     while(1) {
         asm wfi;
     }
}